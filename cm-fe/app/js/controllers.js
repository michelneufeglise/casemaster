'use strict';

/* Controllers */

function UserGroupListCtrl($scope, $http) {
    $http.get('dummyJsonData/usergroups.json').success(function (data) {
        $scope.usergroups = data;
    });
}
